//package com.example.demo;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.crypto.password.NoOpPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//	@Autowired
//	DataSource datasource;
//	
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		
////		auth.inMemoryAuthentication()//authentication
////		.withUser("user")
////		.password("user")
////		.roles("user")
////		.and()
////		.withUser("asdf")
////		.password("asdf")
////		.roles("admin");
//		
//		auth.jdbcAuthentication()
//		.dataSource(datasource)
//		.withDefaultSchema()
//		.withUser(
//				User.withUsername("user")
//				.password("user")
//				.roles("USER")
//				)
//		.withUser(
//				User.withUsername("admin")
//				.password("admin")
//				.roles("ADMIN")
//				);
//		
//		
//	}
//	@Bean
//	public PasswordEncoder getPasswordEncoder() {
//		return NoOpPasswordEncoder.getInstance();//encode the password 
//	}
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		
//		http.authorizeRequests()//authorization
//		.antMatchers("/admin").hasRole("ADMIN")
//		.antMatchers("/user").hasRole("USER")
//		.antMatchers("/").permitAll()
//		//.antMatchers("/**").hasRole("admin")//atmatcher matches the role and give access to admin
//		.and()//end the method chaining
//		.formLogin();//formbase login
//		
//	
//		
//	}
//	
//	
//}
