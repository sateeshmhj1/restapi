package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repository.BookRepository;

import com.example.demo.service.BookService;
import java.util.*;
import com.example.demo.dto.BookDto;
import com.example.demo.mapper.BookMapper;

import com.example.demo.model.Book;

@Service
public class BookServiceImpl implements BookService {
  
  @Autowired
  private BookRepository bookRepository;
  
  @Autowired
  BookMapper bookMapper;
  
  @Override
  public Book addbook(BookDto dto) {
    Book book = bookMapper.toBook(dto);
    book.setCreatedDate(System.currentTimeMillis());
    return bookRepository.save(book);
  }
  
  @Override
  public Book findOne(long id) {
    Optional<Book> book = bookRepository.findById(id);
    if (book.isPresent()) {
      return book.get();
      
    } else {
      return null;
      
    }
    
  }
  
  @Override
  public List<Book> booklist() {
    List<Book> book = bookRepository.findAll();
    if (book.isEmpty()) {
      return null;
    } else {
      return book;
    }
  }
  
  @Override
  public String deletebook(long id) {
    // TODO Auto-generated method stub
    bookRepository.deleteById(id);
    return "{'message':'book deleted'}";
  }

//	@Override
//	public List<Book> booklist() {
//		 
//		return bookRepository.findAll();
//	}
}
