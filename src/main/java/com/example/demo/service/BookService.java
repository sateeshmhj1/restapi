package com.example.demo.service;

import java.util.*;
import com.example.demo.dto.BookDto;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Book;
@Repository
public interface BookService {
	List<Book> booklist();

//	ResponseEntity<Book> booklist();
	
	Book addbook(BookDto dto);
	
	Book findOne(long id);
	
	String deletebook( long id);
}
