package com.example.demo.model;

import com.example.demo.enums.ErrorCode;
import com.example.demo.enums.MessageCode;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ServiceResponce<T> {
	private boolean status = true;
	private MessageCode message;
	private ErrorCode errorCode;
	private T data;
	private Date date =new Date();
	
	public ServiceResponce(MessageCode message){
		this.message = message;
		this.errorCode = null;
		this.status = true;
	}
	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
		this.message = null;
		this.status = true;
		
	}
	public void setData(T data) {
		this.data = data;
		this.status = true;
		
	}
	

}
