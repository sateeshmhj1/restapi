package com.example.demo.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController implements ErrorController{
	
	private static final String path = "/error";

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return path;
	}
	@RequestMapping("/error")
	public String error() {
		return"no mapping ";
	}

}
