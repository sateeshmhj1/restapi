package com.example.demo.controller;

import java.util.*;

import com.example.demo.model.Book;
import com.example.demo.dto.BookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
   
	@Autowired
	private BookService bookService;
	
	 public BookController(BookService bookService ) {
		super();
		this.bookService = bookService;
	}


	@RequestMapping("/delete/{id}")
	public ResponseEntity<?> deletebook(@PathVariable long id) {
		String book = bookService.deletebook(id);
                return ResponseEntity.ok(book);
	}
	
	@RequestMapping("/add")
	public ResponseEntity<Book> addbook(@RequestBody BookDto dto) {
                Book book1 = bookService.addbook(dto);
                return ResponseEntity.ok(book1);
	}
	@RequestMapping("/list/{id}")
	public ResponseEntity<?> findOne(@PathVariable long id){
		 Book book = bookService.findOne(id);
		 return ResponseEntity.ok(book);
		
	}
	
	@RequestMapping("/list")
	public  ResponseEntity<List<Book>> bookList(){
		List<Book> book = bookService.booklist();
		return ResponseEntity.ok(book);
	}
	
	
}
