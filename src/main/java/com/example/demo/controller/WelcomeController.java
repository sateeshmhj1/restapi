package com.example.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {
	
	@RequestMapping("/")
	public String hello() {
		return "hello spring boot";
		
	}
	@RequestMapping("/user")
	public String user() {
		return "hello  user spring boot";
		
	}
	@RequestMapping("/admin")
	public String admin() {
		return "hello  admin spring boot";
		
	}



}
