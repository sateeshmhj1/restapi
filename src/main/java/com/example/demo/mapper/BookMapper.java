
package com.example.demo.mapper;


import org.mapstruct.Mapper;
import com.example.demo.model.Book;
import com.example.demo.dto.BookDto;

@Mapper(componentModel = "spring")
public abstract class BookMapper {
    
public abstract Book toBook(BookDto dto);
    
 
}
